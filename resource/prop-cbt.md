# Système de combat

## phase de jeu découpée en une action chacun: round

* phase de combat incarné par un tour de jeu. le tour est décomposé en round de combat que les protagonistes effectuent chacun leur tour. Donc un round est égal à une action du joueur et de son adversaire, ainsi de suite jusqu'à ce que tous les participants n'est plus de points d'actions.

* l'initiative détermine le joueur qui lancera sa première attaque.

* un joueur disposant de beaucoup de point d'action se voit doté d'une fin de tour plus longue.

* à la fin du tour, les joueurs regagnent leurs points d'actions.


## phase de jeu découpé en phase d'action "complète"

* le joueur obtenant le meilleur score d'initiative se voit doté du premier tour de jeu dans lequel il peut dépenser tous ses points d'actions. Une fois toutes ses actions faites, le tour passe à l'adversaire qui effectuera toutes ses actions.

* 1 round unique par joueur.

* 1 tour = tous les rounds de chaques joueurs.

* Permet une planifacation du tour : pour les mages phases préparatoires...puis déchaîne les enfers : badass !

## choix

* ma préférence va à la 2ème option car elle permet des combos .... WWWWWWWWWWWWWWWIIIIIIIIIIIIIIIIII !