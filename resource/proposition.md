#  SYSTEME de seuil

* type shadowrun ou GoT, les caracs sont sous formes de points représentant le nombre de DD à lancer. Les DD sont onsidérés comme validés selon le seuil qu'ils ont atteints (4+ ou 5+ etc...), et c'est le nombre de réussite qui est confronté à la difficulté de l'action réalisé (3 ou 4 réussite par exemple).

> système cool et efficace mais que je maîtrise moins, à tester.

* après réunion du 06/07/2018, il a été convenu d'adopter ce système, à utiliser avec 3 caracs de bases : 

> L'intelligence/sagesse
> L'agilité/dextérité
> La force/endurance

* nous avons retenu le système de compétences débloquable par des seuils avec des combinaisons de caracs.

Je (PE) propose une variante du système de seuil sur 5 niveaux que j'avais utilisé il y a un bout de temps.

1. Profane
2. Novice
3. Disciple
4. Adepte
5. Maitre

## Bref

* je propose une construction de classe en pyramide pour les personnages : une classe Entities regroupant les caracs fondamentales de tous personnages PJ ou PNJ. Puis une classe joueur, héritant de celle ci et une autre classe PNJ. 
(Guillaume/08-07-2018);