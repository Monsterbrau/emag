import { Case } from './mapCase';

"use strict";

export class Level {
  constructor(level) {
    this.level = level;
  }
  generate() {
    let grid = [];
    for (let x = 0; x < this.level; x++) {
      grid[x] = [];
      for (let y = 0; y < this.level; y++) {
        let map = grid[x][y];
        map = new Case(x, y);
        map.randomEvent = map.eventGeneration();
        grid[x].push(map);
      };
    }
    return grid;
  }
}
